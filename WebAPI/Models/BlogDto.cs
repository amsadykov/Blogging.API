﻿namespace WebAPI.Models
{
    public class BlogDto
    {
        public int BlogId { get; set; }
        public string Url { get; set; }
    }
}
